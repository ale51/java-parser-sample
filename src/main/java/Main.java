import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.type.PrimitiveType;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import model.File;
import model.StandardOutputFormat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        String targetRemoteBranchName = "origin/master";
        if (args.length != 0) {
            targetRemoteBranchName = args[0];
        }

        String[] Command = {"git", "diff", "HEAD.." + targetRemoteBranchName, "--name-only"};
        List<File> fileList = new ArrayList<>();
        System.out.println(Arrays.stream(Command).collect(Collectors.joining(" ")));

        Runtime runtime = Runtime.getRuntime();
        try {
            Process process = runtime.exec(Command);
            InputStream inputStream = process.getInputStream();
            Scanner scanner = new Scanner(inputStream);

            while (scanner.hasNext()) {
                fileList.add(new File(scanner.next()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        fileList.stream()
                .filter((l -> l.isJavaFile()))
                .filter((l -> !l.isGeneratedFile()))
                .forEach(l -> {
                    System.out.println(l.getPath());

                    java.io.File file = new java.io.File(l.getPath());
                    CompilationUnit cu = null;
                    try {
                        cu = StaticJavaParser.parse(file);
                        VoidVisitor<String> visitor = new SomeVisitor();
                        cu.accept(visitor, l.getPath());

                        System.out.println();

                    } catch (FileNotFoundException e) {
                        System.out.println("削除したファイル");
                    }
                });
    }

    private static class SomeVisitor extends VoidVisitorAdapter<String> {

        @Override
        public void visit(MarkerAnnotationExpr md, String arg) {
            super.visit(md, arg);

            /**
             * Autowired not recommended
             */
            if ("Autowired".equals(md.getName().toString())) {

                System.out.println("@Autowired is not recommended. Use @RequiredArgsConstructor instead.");

            }

            /**
             * final keyword on RequiredArgsConstructor
             */
            if ("RequiredArgsConstructor".equals(md.getName().toString())) {

                if (md.getParentNode().isPresent()) {
                    List<Node> nodeList = md.getParentNode().get().getChildNodes();
                    nodeList.stream()
                            .filter(l -> l instanceof FieldDeclaration)
                            .forEach(l -> {
                                if (!l.toString().contains("final")) {
                                    System.out.println("Add final keyword when @RequiredArgsConstructor is used");
                                }
                            });
                }
            }
        }


        @Override
        public void visit(MethodDeclaration md, String arg) {
            super.visit(md, arg);

            /**
             * @Override should be added
             */
            if (arg.contains("Repository") || arg.contains("Service")) {
                List<Node> nodeList = md.getChildNodes();
                nodeList.stream()
                        .filter(l -> l instanceof Modifier)
                        .filter(l -> l.toString().contains("public"))
                        .forEach(l -> {
                            l.getParentNode().ifPresent(m->{
                                boolean b = m.getChildNodes()
                                        .stream()
                                        .filter(n -> n instanceof MarkerAnnotationExpr)
                                        .noneMatch(n -> n instanceof MarkerAnnotationExpr && n.toString().equals("@Override"));
                                if(b){
                                    StandardOutputFormat standardOutputFormat = new StandardOutputFormat(arg, md.getRange(), "Add @Override", "");
                                    standardOutputFormat.print();
                                }
                            });
                        });
            }

            /**
             * Run on different thread from netty's one.
             */
            if (arg.contains("Controller")) {

                List<Node> nodeList = md.getChildNodes();
                nodeList.stream()
                        .filter(l -> l instanceof BlockStmt)
                        .flatMap(l -> {
                            return l.getChildNodes().stream();
                        })
                        .filter(l -> l instanceof ReturnStmt)
                        .filter(l -> !l.toString().contains("defer"))
                        .filter(l -> {
                            List<Node> list = l.getParentNode().get().getParentNode().get().getChildNodes();

                            Node node = list.stream().filter(m -> m instanceof Modifier).findFirst().get();
                            return node.toString().contains("public");
                        })
                        .forEach(l -> {

                            StandardOutputFormat standardOutputFormat = new StandardOutputFormat(arg, md.getRange(), "Use Mono.defer()", "");
                            standardOutputFormat.print();

                        });
            }
        }

        @Override
        public void visit(PrimitiveType md, String arg) {
            super.visit(md, arg);

            /**
             * Primitive type is not recommended
             */
            StandardOutputFormat standardOutputFormat = new StandardOutputFormat(arg, md.getRange(), "Primitive Type is not recommended. Use Class Type instead.", "cf. http://gitlab.cloud.ceres.local/mochiie/core/merge_requests/676#note_36511");
            standardOutputFormat.print();
        }
    }
}
