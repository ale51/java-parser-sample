package model;

public class File {
    private final String path;

    public File(String path) {
        this.path = path;
    }

    public boolean isJavaFile() {
        return "java".equals(this.getExtension());
    }

    public boolean isGeneratedFile() {
        return this.path.contains("modelgen") || this.path.contains("mappergen");
    }

    public String getPath() {
        return this.path;
    }

    private String getExtension() {
        int point = this.path.lastIndexOf(".");
        return path.substring(point + 1);
    }
}
