package model;

import com.github.javaparser.Range;

import java.util.Optional;

public class StandardOutputFormat {
    private String filePath;
    private Optional<Range> range;
    private String message;
    private String detail;

    public StandardOutputFormat(String filePath, Optional<Range> range, String message, String detail) {
        this.filePath = filePath;
        this.range = range;
        this.message = message;
        this.detail = detail;
    }

    public void print() {

        String str = "";
        str += "- " + filePath + "\n";
        if (range.isPresent()) {
            str += "line: " + range.get().begin.line + "\n";
            str += "col: " + range.get().begin.column + "\n";
        }
        str += "message: " + message + "\n";
        str += "info: " + detail + "\n";

        System.out.println(str);
    }

}
