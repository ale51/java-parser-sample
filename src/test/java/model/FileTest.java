package model;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.*;

public class FileTest {

    private File javaFile;
    private File javaScriptFile;
    private File modelgenFile;
    private File mappergenFile;

    @Before
    public void init() {
        this.javaFile = new File("/tmp/Main.java");
        this.javaScriptFile = new File("/tmp/Main.js");
        this.modelgenFile = new File("/tmp/modelgen/Sample.java");
        this.mappergenFile = new File("/tmp/mappergen/Sample.java");
    }

    @Test
    public void isJavaFile() {

        assertTrue(this.javaFile.isJavaFile());
        assertFalse(this.javaScriptFile.isJavaFile());

    }

    @Test
    public void isGeneratedFile() {

        assertTrue(this.modelgenFile.isGeneratedFile());
        assertTrue(this.mappergenFile.isGeneratedFile());
        assertFalse(this.javaScriptFile.isGeneratedFile());

    }

    @Test
    public void getPath() {

        assertTrue("/tmp/Main.java".equals(this.javaFile.getPath()));

    }
}
